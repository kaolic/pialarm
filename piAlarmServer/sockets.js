var socketio = require('socket.io')

module.exports = function(http){
    io = socketio(http);

    // socket.io namespaces
    ui = io.of('/ui');
    pi = io.of('/pi');


    // ui sockets
    ui.on('connection', function(socket){
        console.log("UI Connected!!");

        socket.on('getState', function() {
          console.log("ui.getState");
          pi.emit('getState');
        });

        socket.on('toggleState', function(data) {
          console.log("ui.toggleState");
          pi.emit('toggleState', data);
        });

        socket.on('capture', function(data) {
          console.log("capture called! with name: " + data.name);
          pi.emit('capture', data);
        });

        socket.on('disconnect', function() {
          console.log("UI Disconnected!");
        });
    });


    // pi sockets
    pi.on('connection', function(socket){
        console.log("PI Connected!!");

        socket.on('getState', function(data) {
          console.log("pi.getState");
          ui.emit('getState', data);
        });

        socket.on('toggleState', function(data) {
          console.log("pi.toggleState");
          ui.emit('toggleState', data);
        });

        socket.on('capture', function(data) {
          console.log("pi.capture");
          ui.emit('capture', data);
        });

        socket.on('disconnect', function() {
          console.log("PI Disconnected!!");
        });
    });

    return io;
}

//io.sockets.emit('signal', obj)
