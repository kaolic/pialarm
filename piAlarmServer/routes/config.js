var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
  res.render('config', { title: 'piAlarm', activePage: 'config' });
});

module.exports = router;
