var app = angular.module('app', ['cgNotify', 'blockUI']);

// socket factory
app.factory('$socket', function() {
  console.log("socket factory!");
  var socket = io('/ui');

  socket.on('error', function(err) {
    console.log(err);
  });

  return socket;
});


// root controller
app.controller('homeCtrl', ['$scope', '$socket', 'notify', 'blockUI', function($scope, $socket, notify, blockUI) {

  $scope.alarms = [];
  var captureBlock = blockUI.instances.get('captureBlock');

  function init() {
    //notify config
    notify.config({ "duration": 3000});

    blockUI.start("Waiting for alarms to respond...");
    notify({"message": "Getting alarm nodes", "classes": ""});
    $socket.emit('getState');
  }

  function getAlarmWithName(name) {
    for(var i = 0; i < $scope.alarms.length; i++) {
      if($scope.alarms[i].name == name) {
        return $scope.alarms[i];
      }
    };
    return null;
  }

  $scope.toggle = function(name, currState, newState) {
    if(currState != newState) {
      $socket.emit('toggleState',
        {"name": name, "state": newState});
    }
  };

  $scope.capture = function(name) {
    console.log("Capture photo name: " + name);
    captureBlock.start("Capturing...");
    $socket.emit('capture', { "name": name });
  };

  $socket.on('getState', function(data) {
    if(getAlarmWithName(data.name) == null) {
      $scope.$apply(function() {
        notify("Alarm with name (" + data.name + ") added!");
        $scope.alarms.push(data);
        blockUI.stop();
      });
    }
  });

  $socket.on('toggleState', function(data) {
    $scope.$apply(function() {
      var alarm = getAlarmWithName(data.name);
      alarm.enabled = data.newState;
    });
  });

  $socket.on('capture', function(data) {
    $scope.$apply(function() {
      var alarm = getAlarmWithName(data.name);
      alarm.image = "data:image/jpeg;base64," + data.image
      captureBlock.stop();
    });
  });

  $socket.on('motion', function(data) {
    //Show notification
  });

  init();
}]);


// config controller
app.controller('configCtrl', ['$scope', '$socket', function($scope, $socket) {

}]);
