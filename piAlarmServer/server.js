var app = require('./app');
var http = require('http').Server(app);
var io = require('./sockets')(http);

http.listen(3000, function() {
  console.log("Server listening...");
});
