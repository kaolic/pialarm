var http = require('http').createServer();
var io = require('socket.io')(http);

var pi = io.of('/pi');

pi.on('connection', function(socket) {
  console.log("piMotionAlarmClient connected!");

  socket.emit('get');

  socket.on('get', function(data) {
    console.log(data);
  });

  socket.emit('setEnabled', false);

  socket.emit('get');

  socket.emit('setName', 'MotionTest');

  socket.emit('get');

  socket.emit('getImage');

  socket.on('getImage', function(data) {
    console.log(data);
  });
});


http.listen(3000);
