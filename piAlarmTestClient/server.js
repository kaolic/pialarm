var fs = require('fs');
var util = require('util');
var config = require('./config.json');
var socket = require('socket.io-client')(config.serverUrl.concat(config.socketNamespace));

socket.on('connect', function() {
  console.log("Connected to server");
});

socket.on('disconnect', function() {
  console.log('disconnected!');
});

socket.on('error', function(err) {
  console.log(err);
});

socket.on('getState', function() {
  console.log("getState called");

  socket.emit('getState',
  {
    "name": config.name,
    "enabled": config.enabled,
    "image": ""
  });
});

socket.on('toggleState', function(data) {
  console.log(util.format("toggleState called with data: name %s, new state value %s", data.name, data.state));

  if(isMe(data.name)) {
      config.enabled = data.state;
      writeConfig();
      socket.emit('toggleState',
        {"name": data.name, "newState": config.enabled});
  }
});

socket.on('capture', function(data) {
  console.log("capture called!");

  if(isMe(data.name)) {
    fs.readFile('test.jpg', function(err, image) {
      var base64data = new Buffer(image).toString('base64');
      socket.emit('capture',
        { "name": data.name, "image": base64data });
    });
  }
});

function isMe(name) {
  return name == config.name;
}

function writeConfig() {
  fs.writeFile('config.json', JSON.stringify(config, null, 4), function(err) {
    if(err) {
      console.log(err);
    } else {
      console.log("config saved to config.json");
    }
  });
};
